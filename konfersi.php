<!DOCTYPE html>
<?php 
session_start();
include 'koneksi.php';
$query=$conn->query("SELECT * FROM `tb_semester`");
 ?>

<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Beranda</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
<body>

  <div>
    <nav class="navbar navbar-inverse">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toogle="collapse" data-target="#target-list">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span> 
        </button>
        <a href="#" class="navbar-brand">SIFMA</a>
      </div>

      <div class="collapse navbar-collapse" id="target-list">
        <ul class="nav navbar-nav">

          <li class="active"><a href="#">Home</a></li>

          <?php 
          $level = $_SESSION['level'] == 'user';
          if($level){
          ?>

          <li><a href="#">Forum</a></li>

          <?php }else{ ?>
          <li><a href="#">File</a></li>
          <li><a href="file/home.php">Upload</a></li>
          <?php } ?>
          <li><a href="konfersi.php">Konfersi Gambar</a></li>
          <li><a href="logout.php">logout</a></li>
         </ul>
        <form role="search" class="navbar-form navbar-right">
        <div class="form-group">
          <input type="text" class="form-control" placeholder="Cari...">
          <button type="submit" class="btn btn-primary">Cari</button>
        </div>
        </form>
      </div>
  </div>


  <div class="col-md-6">
<h3>Konfersi Gambar</h3>

<table class="table table-bordered">
  <thead>
    <tr>
      <th>Gambar</th>
      <th>Pilih Gambar</th>
      <th>hasil</th>
    </tr>
  </thead>

  <tbody>
    <tr>
      <td></td>
      <td><a class="btn btn-info">Pilih File</a></a></td>
      <td><input type="textarea" name=""></td>
    </tr>
  </tbody>

</table>
</div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>