<?php
session_start();
include 'koneksi.php';
$query=$mysqli->query("SELECT * FROM `tb_barang`");
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Beranda</title>
     </head>
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
</head>

<body>

<div>
    <nav class="navbar navbar-inverse">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toogle="collapse" data-target="#target-list">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span> 
        </button>
        <a href="#" class="navbar-brand">SIFMATI</a>
      </div>

      <div class="collapse navbar-collapse" id="target-list">
        <ul class="nav navbar-nav">

          <li class="active"><a href="#">Home</a></li>

          <?php
          $level = $_SESSION['level'] == 'user';
          if($level){
          ?>

          <li><a href="#">Forum</a></li>

          <?php }else{ ?>
          <li><a href="../beranda.php">File</a></li>
          <li><a href="home.php">Upload</a></li>
          <?php } ?>
          <li><a href="../konfersi.php">Konfersi Gambar</a></li>
          <li><a href="../logout.php">logout</a></li>
         </ul>
        <form role="search" class="navbar-form navbar-right">
        <div class="form-group">
          <input type="text" class="form-control" placeholder="Cari...">
          <button type="submit" class="btn btn-primary">Cari</button>
        </div>
        </form>
      </div>
  </div>

<div class="col-md-6">
<h3>Upload File
</h3>
<table class="table table-bordered">
	<thead>
		<tr>
			<th>Mata Kuliah</th>
			<th>Semester</th>
			
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
	<?php while ($data=mysqli_fetch_array($query)) {
		# code...
	 ?>
		<tr>
			<td><?=$data['nama_barang']?></td>
			<td><?=$data['semester']?></td>
			<td><a href="edit_barang.php?id=<?=$data['id_barang']?>" class="btn btn-info">Edit</a> <a href="delete.php?id=<?=$data['id_barang']?>" class="btn btn-danger">Hapus</a></td>

		</tr>
		<?php }?>
	</tbody>
</table>
<a href="add_barang.php" class="btn btn-info">Tambah</a>
</div>
</body>
</html>