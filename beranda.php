<!DOCTYPE html>
<?php 
session_start();
include 'koneksi.php';
$query=$conn->query("SELECT * FROM `tb_semester`");
 ?>

<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Beranda</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
<body>

  <div>
    <nav class="navbar navbar-inverse">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toogle="collapse" data-target="#target-list">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span> 
        </button>
        <a href="#" class="navbar-brand">SIFMA</a>
      </div>

      <div class="collapse navbar-collapse" id="target-list">
        <ul class="nav navbar-nav">

          <li class="active"><a href="#">Home</a></li>

          <?php 
      	 	$level = $_SESSION['level'] == 'user';
      	 	if($level){
	 	      ?>

          <li><a href="#">Forum</a></li>

          <?php }else{ ?>
          <li><a href="#">File</a></li>
          <li><a href="file/home.php">Upload</a></li>
          <?php } ?>
          <li><a href="konfersi.php">Konfersi Gambar</a></li>
	     	  <li><a href="logout.php">logout</a></li>
         </ul>
        <form role="search" class="navbar-form navbar-right">
        <div class="form-group">
          <input type="text" class="form-control" placeholder="Cari...">
          <button type="submit" class="btn btn-primary">Cari</button>
        </div>
        </form>
      </div>
  </div>

<div class="container-fluid">
        <div class="jumbotron">
         <h1>Selamat datang di SIFMA </h1> <h2>(Sistem informasi File Teknik Informatika)</h2>
         <p>Tempatnya file kampus,kami menyediakan file materi perkuliahan di UMSIDA anda bisa mendownloadnya secara gratis tanpa biasaya (selama mempunyai kuota).</p>
         <a href="#" class="btn btn-success" role="button"> Lihat Kelas</a>
        </div>

        <div class="row">
<?php while ($tampil=mysqli_fetch_array($query)) {
  # code...
 ?>
            <div class="col-md-3">
            <div class="thumbnail">
              <img src="img/1.jpg" alt="">
              <div class="caption">
                <h3><?=$tampil['semester']?></h3>
                <h4>SEMESTER</h4>
                <a href="detail.php?id=<?=md5($tampil['semester'])?>" class="btn btn-primary"> Lihat Sekarang </a>
              </div>
            </div>
          </div>
<?php }?>

        </div> <!-- akhir thumbnails row -->

        <div class="alert alert-success" role="alert"> Lihat contoh kelas Sekloahkoding</div>

        <div class="row">
         <div class="col-md-6">

          <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#home" aria-controller="home" role="tab" data-toggle="tab">Home</a></li>
            <li role="presentation" ><a href="#forum" aria-controller="forum" role="tab" data-toggle="tab">Forum</a></li>
          </ul>

          <div class="tab-content">
            <div role="tabpabel" class="tab-pane active" id="home">File lebih mudah jika disimpan.</div>
          </div>
           <div role="tabpabel" class="tab-pane active" id="forum">jangan sampai hilang seperti mantan yang sudah ke palaminan.</div>
          </div>

          <div>
            <div class="col-md-6">
              <div class="embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item" src="https://www.youtube.com/watch?v=XRAEPLCoYFc"></iframe>
              </div>
            </div>
          </div>

         </div>
          
        </div >

      </div> <!-- akhir kontainer -->
      <br><br><br>
        
  

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>